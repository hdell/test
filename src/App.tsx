import {useState} from 'react';
import { Button, TextField } from '@mui/material';
import './App.css';
import styled from 'styled-components'

const StyledDiv = styled.div`
  display: flex;
  justify-content: center;
`

const StyledButton = styled(Button)`
  &.MuiButtonBase-root {
    margin-right: 25px;
  }
`

function App() {

  const [textValue, setTextValue] = useState("");  

  const handleTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTextValue(event.target.value);
  };

  const handleClick = () => {
    console.log("Value: ", textValue);
  };

  return (
    <StyledDiv>
      <StyledButton variant="outlined" onClick={handleClick}>Get Weather</StyledButton>
      <TextField id="standard-basic" label="Standard" variant="standard" onChange={handleTextChange} />
    </StyledDiv>
  );
}

export default App;
