# This is a test project

### Tech Stack, list of Libraries and tools used.

1. Webpack
2. Yarn
3. Typscript
4. MSW
5. Cypress
6. Gitlab
7. Docker
8. Terraform
9. AWS
   * (AWS Services used)
   * Lambda
   * S3

---
## Dependencies

**Gitlab** is used for CI/CD install the gitlab-runner to execute pipelines locally.

```brew install gitlab-runner```

***Cypress*** is a testing library the supports e2e and also comes with its own IDE.

```yarn add cypress```

---
## Usage

### Gitlab
To run CI/CD locally
```gitlab-runner exec shell test``` to initiate the test job defined in ***.gitlab-ci.yml***

### Cypress
To open the Cypress IDE and run tests 
```yarn run cypress open``` . To run the tests headless (without the UI and just in the the terminal) ```yarn cypress run```